#!/bin/bash

if [ $# -ne 2 ];then
    echo "$0 Nom_cluster chemin/vers/Application"
    exit 2
fi

IFS="/"
App_path=$2
Cluster_Name=$1
Name=$(basename "$App_path" | sed 's/\.ear$//')
user="dmgr02"
password="dmgr02"

IFS="\n"
output=$(./wsadmin.sh -lang jython -username $user -password $password -c "AdminApp.list()")
if  echo "$output" | grep -qi "$Name" ;then
        echo "l'application existe deja "

        status=$(./wsadmin.sh -lang jython -username $user -password $password -c "print AdminControl.completeObjectName('type=Application,name=$Name,*')" | grep -v "WASX")

        if [ -n "$status" ]; then
                echo "Application $Name est demarer"
        else
                echo "Application $Name n'est pas demarer"
                start=$(./wsadmin.sh -lang jython -username $user -password $password -c "AdminApplication.startApplicationOnCluster('$Name','$Cluster_Name')")
                echo "Application $Name est demarer"

        fi
else
        echo "l'application n'existe pas"

        deploy=$(./wsadmin.sh -lang jython -username $user -password $password -c "AdminApp.install('$App_path', '[-cluster $Cluster_Name]')")
        echo "application deployer"
        save=$( ./wsadmin.sh -lang jython -username $user -password $password -c "AdminConfig.save()")
        echo "application sauvgarder"
        start=$(./wsadmin.sh -lang jython -username $user -password $password -c "AdminApplication.startApplicationOnCluster('$Name','$Cluster_Name')")
        echo "demarage de l'application"
fi

